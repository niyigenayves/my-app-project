// 1 - Import Node.js core module
var http = require('http');  

 // 2 - creating server //handle incomming requests here..

var server = http.createServer(function (req, res) { 
    if (req.url == '/') { //check the URL of the current request // set response header 
        res.writeHead(200, { 'Content-Type': 'text/html' }); // set response content 
        res.write('<html><body><p>This is home Page.</p></body></html>');
        res.end(); 
    } else if (req.url == "/student") { 
        res.writeHead(200, { 'Content-Type': 'text/html' }); 
        res.write('<html><body><p>This is student Page.</p></body></html>'); 
        res.end(); 
    }else if(req.url == "/data"){
        let data ={
            name:"Emmanuel Dufite",
            age:16,
            gender:"Male",
            class:"B"
        }
        res.writeHead(200, { 'Content-Type': 'application/json' }); 
        res.write(JSON.stringify(data)); 
        res.end();
    }else {
        res.writeHead(404, { 'Content-Type': 'text/html' }); 
        res.write('<html><body><p>Invalid path.</p></body></html>'); 
        res.end(); 
    }
}); 
//3 - listen for any incoming requests 
server.listen(2000); 
console.log('Node.js web server at port 2000 is running..');
